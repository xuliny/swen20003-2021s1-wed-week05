import bagel.Image;
import bagel.Window;
import bagel.util.Point;
import bagel.util.Vector2;

/**
 * Xulin Yang, 904904
 *
 * @create 2021-03-31 23:01
 * description:
 **/

public class Ball {
    private static final String BALL_IMAGE = "res/ball.png";
    private final Image ballImage = new Image(BALL_IMAGE);

    // (a) The player (in Question 3) needs to catch a moving ball (res/ball.png). When the game starts,
    //the ball should choose a random position on the window to be drawn at and a random direction.
    //Set the step size of the ball to 0.5.
    private static final double STEP_SIZE = 0.5;

    private double x, y;
    private double directionX, directionY;

    public Ball() {
        // When the game starts,
        //    the ball should choose a random position on the window to be drawn at and
        //                           a random direction.
        resetPosition();
        resetDirection();
    }

    public void resetDirection() {
        // a vector of 2 random variable
        Vector2 d = new Vector2(Math.random(),Math.random());
        // normalised so that won't be updated too much
        directionX = d.normalised().x;
        directionY = d.normalised().y;
    }

    public void resetPosition() {
        x = Math.random() * Window.getWidth();
        y = Math.random() * Window.getHeight();
    }

    public Point getPosition() {
        return new Point(x, y);
    }

    public void update() {
        // (d) Extend the code to make the ball choose a random diagonal direction, and move in that direction.
        //  - When the ball reaches the left and right edges of the window, it should reverse horizontal direction.
        //  - Similarly, when the ball reaches the top and bottom edges of the window, it should reverse vertical direction.
        //      You can change the step size of player and ball when simulating.
        // When the ball reaches the left and right edges of the window, it should reverse horizontal direction.
        if (x < 0 || x > Window.getWidth()) {
            directionX *= -1;
        }
        // Similarly, when the ball reaches the top and bottom edges of the window, it should reverse vertical direction.
        if (y < 0 || y > Window.getHeight()) {
            directionY *= -1;
        }

        // (a)
        x += directionX * STEP_SIZE;
        y += directionY * STEP_SIZE;
        ballImage.draw(x, y);
    }
}
