import bagel.*;
import bagel.util.Point;

import java.text.DecimalFormat;

/**
 * Xulin Yang, 904904
 *
 * @create 2021-03-31 22:53
 * description:
 **/

public class Q4 extends AbstractGame {
    private static final String PLAYER_IMAGE = "res/player.png";
    private static final String BALL_IMAGE = "res/ball.png";
    private static final Point PLAYER_POSITION = new Point(200, 350);
    private static final Point BALL_POSITION = new Point(650, 180);

    private final Image playerImage = new Image(PLAYER_IMAGE);
    private final Image ballImage = new Image(BALL_IMAGE);

    private static final double STEP_SIZE = 2;

    private double playerX = 10; // PLAYER_POSITION.x;
    private double playerY = PLAYER_POSITION.y;

    private static final double SCORE_DISTANCE = 20;

    private final Font font = new Font("res/conformable.otf", 24);
    private static final Point FONT_POSITION = new Point(32, 32);

    // Now, we are revising the catch the Ball game so that the player moves toward the ball step by step. In
    //this question, you will learn how to set the player's moving direction and how to let the player move
    //towards this direction by one step. Let the movement of the player controlled by the Enter key, not
    //the arrow keys:
    // (0) If the Enter key is pressed (use Input.wasPressed method), do the following:

    // (a) Calculate the direction (xd; yd) pointing from the player to the ball: for (x1; y1) and (x2; y2) being
    // the positions of the player and the ball respectively, set:
    //      xd = (x2-x1) / Math.sqrt(Math.pow(x1-x2, 2) + Math.pow(y1-y2, 2))
    //      yd = (y2-y1) / Math.sqrt(Math.pow(x1-x2, 2) + Math.pow(y1-y2, 2))
    private double playerDirectionX = 0,
                   playerDirectionY = 0;

    // (b) Moving in direction (xd; yd) by one step:
    //      add STEP_SIZE * xd to x1;
    //      add STEP_SIZE * yd to y1;

    // (c) After each movement, print to the console the position of the player in the form of
    //                                  x-coordinate, y-coordinate
    //Keep 2 decimal digits for both x-coordinate and y-coordinate. To do so, you can use the format
    //method in java.text.DecimalFormat class, e.g., define a static attribute
    private static DecimalFormat df = new DecimalFormat("0.00");
    //and use it as
    //      System.out.println(df.format(playerX) +"," + df.format(playerY));

    /**
     * calculate the direction for the player based on the input point w.r.t. player's location
     * @param Dest destination for the player to move to
     */
    public void setPlayerDirectionTo(Point Dest){
        double Len = new Point(playerX, playerY).distanceTo(Dest);
        playerDirectionX = (Dest.x - playerX)/Len;
        playerDirectionY = (Dest.y - playerY)/Len;
    }

    public static void main(String[] args) {
        new Q4().run();
    }

    @Override
    protected void update(Input input) {
        if (input.wasPressed(Keys.ESCAPE)) {
            System.out.println("closed");
            Window.close();
        } else {
            // (0)
            if (input.wasPressed(Keys.ENTER)) {
                // (a)
                this.setPlayerDirectionTo(BALL_POSITION);
                // (b)
                playerX += STEP_SIZE * this.playerDirectionX;
                playerY += STEP_SIZE * this.playerDirectionY;
                // (c)
                System.out.println(df.format(playerX) + "," + df.format(playerY));
            }

            if (new Point(playerX, playerY).distanceTo(BALL_POSITION) <= SCORE_DISTANCE) {
                // System.out.println("Great job!");
                font.drawString("Great job!", FONT_POSITION.x, FONT_POSITION.y);
            }
        }


        playerImage.draw(playerX, playerY);
        ballImage.draw(BALL_POSITION.x, BALL_POSITION.y);
    }
}
