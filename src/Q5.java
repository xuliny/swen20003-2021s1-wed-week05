import bagel.*;
import bagel.util.Point;

import java.text.DecimalFormat;

/**
 * Xulin Yang, 904904
 *
 * @create 2021-03-31 22:59
 * description:
 **/

public class Q5 extends AbstractGame {
    private static final String PLAYER_IMAGE = "res/player.png";

    private static final Point PLAYER_POSITION = new Point(200, 350);

    private final Image playerImage = new Image(PLAYER_IMAGE);

    private static final double STEP_SIZE = 20;

    private double playerX = 10; // PLAYER_POSITION.x;
    private double playerY = PLAYER_POSITION.y;

    private static final double SCORE_DISTANCE = 20;

    private final Font font = new Font("res/conformable.otf", 24);
    private static final Point FONT_POSITION = new Point(32, 32);

    private static DecimalFormat df = new DecimalFormat("0.00");

    private double playerDirectionX = 0,
                   playerDirectionY = 0;
    public void setPlayerDirectionTo(Point Dest){
        double Len = new Point(playerX, playerY).distanceTo(Dest);
        playerDirectionX = (Dest.x - playerX)/Len;
        playerDirectionY = (Dest.y - playerY)/Len;
    }

    // (a) The player (in Question 3) needs to catch a moving ball (res/ball.png). When the game starts,
    //the ball should choose a random position on the window to be drawn at and a random direction.
    //Set the step size of the ball to 0.5.
    //      see Ball.java
    private Ball ball = new Ball();

    // (b) When the player is within 20 pixels of the ball, the ball should move to another random position.

    // (c) Keep track of the player's score, which starts at 0 and increases by 1 every time the player catches
    //      the ball. Draw this score (using Font) at the top-left of the window.
    private int score = 0;

    public static void main(String[] args) {
        new Q5().run();
    }

    @Override
    protected void update(Input input) {
        if (input.wasPressed(Keys.ESCAPE)) {
            System.out.println("closed");
            Window.close();
        } else {
            if (input.wasPressed(Keys.ENTER)) {
                // (a)
                this.setPlayerDirectionTo(ball.getPosition());
                playerX += STEP_SIZE * this.playerDirectionX;
                playerY += STEP_SIZE * this.playerDirectionY;
                System.out.println(df.format(playerX) + "," + df.format(playerY));
            }

            // (b)
            if (new Point(playerX, playerY).distanceTo(ball.getPosition()) <= SCORE_DISTANCE) {
                ball.resetPosition();

                // (c)
                score++;
            }
            // (c)
            font.drawString("Score: " + score, FONT_POSITION.x, FONT_POSITION.y);
        }

        playerImage.draw(playerX, playerY);
        // (a)
        ball.update();
    }

    // Additional exercise:
    //      1. create a Player.java class just like you abstract the Ball
    //      2. use up, down, left, right to control the player, stop player moving if it is going to be outside of the window
    //      3. select some if condition block in update method and
    //             - right click it -> refactor -> Extract -> Method
    //         in order to improve the readability of your code :)
    // SWEN20003 is fun!
}
