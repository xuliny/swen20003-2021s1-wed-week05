import bagel.*;
import bagel.util.Point;

/**
 * Xulin Yang, 904904
 *
 * @create 2021-03-31 22:48
 * description:
 **/

public class Q3 extends AbstractGame {
    // (a) When the game starts, the player should be rendered to the screen at the position: (200, 350),
    //and the ball should be rendered at position: (650, 180). You can use the Point class to define
    //the position.
    // - The player image is located at res/player.png, and the ball image is located at res/ball.png.

    private static final String PLAYER_IMAGE = "res/player.png";
    private static final String BALL_IMAGE = "res/ball.png";
    private static final Point PLAYER_POSITION = new Point(200, 350);
    private static final Point BALL_POSITION = new Point(650, 180);

    private final Image playerImage = new Image(PLAYER_IMAGE);
    private final Image ballImage = new Image(BALL_IMAGE);

    // (b) The player should be able to move left, right, up, and down, using the respective arrow keys at a
    //constant step size (in pixels per frame). Try different values for the step size (a constant named as
    //STEP SIZE), starting at 1.
    private static final double STEP_SIZE = 1;

    private double playerX = PLAYER_POSITION.x;
    private double playerY = PLAYER_POSITION.y;

    // (c) If the player comes within 20 pixels of the ball, we say the player catches the ball and you should
    // print to the console "Great job!". Remember, the Euclidean distance between points (x1; y1) and
    // (x2; y2) is given by: d_12 = Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2))
    private static final double SCORE_DISTANCE = 20;

    // (d) Instead of printing the greeting to the console, draw the text to the screen using the font provided
    // (res/conformable.otf). Draw it in size 24 font at coordinate (32, 32). (See the Font class in Bagel
    // documentation.)
    private final Font font = new Font("res/conformable.otf", 24);
    private static final Point FONT_POSITION = new Point(32, 32);

    public static void main(String[] args) {
        new Q3().run();
    }

    @Override
    protected void update(Input input) {
        // (b)
        if (input.isDown(Keys.LEFT)) {
            playerX -= STEP_SIZE;
        }
        if (input.isDown(Keys.RIGHT)) {
            playerX += STEP_SIZE;
        }
        if (input.isDown(Keys.UP)) {
            playerY -= STEP_SIZE;
        }
        if (input.isDown(Keys.DOWN)) {
            playerY += STEP_SIZE;
        }

        // (d)
        if (new Point(playerX, playerY).distanceTo(BALL_POSITION) <= SCORE_DISTANCE) {
             // System.out.println("Great job!");
            font.drawString("Great job!", FONT_POSITION.x, FONT_POSITION.y);
        }

        playerImage.draw(playerX, playerY);
        ballImage.draw(BALL_POSITION.x, BALL_POSITION.y);
    }
}
